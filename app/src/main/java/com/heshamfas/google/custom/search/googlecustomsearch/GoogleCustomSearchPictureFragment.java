package com.heshamfas.google.custom.search.googlecustomsearch;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Hesham on 3/12/2016.
 */
public class GoogleCustomSearchPictureFragment extends DialogFragment implements
        View.OnClickListener{

    ImageLoader mImageLoader = GoogleCustomSearch.getInstance().getImageLoader();
    NetworkImageView mNetworkImageView;
    Button mCloseBtn ;
    private URL imageUrl;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return View.inflate(getActivity(),R.layout.picture_fragment,container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNetworkImageView = (NetworkImageView)view.findViewById(R.id.niv_picture_fragment_picture);
        mCloseBtn = (Button) view.findViewById(R.id.btn_search_picture_close);
        mCloseBtn.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().setCanceledOnTouchOutside(false);
        Bundle arguments = getArguments();
        String url = arguments.getString("url");
        if(!TextUtils.isEmpty(url)){
            try {
                imageUrl = new URL(url);
                mNetworkImageView.setImageUrl(url,mImageLoader);
            } catch (MalformedURLException e) {
                showNoPictureLayout();
                e.printStackTrace();
            }
        }
    }

    private void showNoPictureLayout(){
        // ToDo work in progress
        //show when url fails
        // will be a FrameLayout Showing no pictures
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
