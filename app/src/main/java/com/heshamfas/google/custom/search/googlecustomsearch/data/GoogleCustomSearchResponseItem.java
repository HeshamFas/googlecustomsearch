package com.heshamfas.google.custom.search.googlecustomsearch.data;

import java.util.PriorityQueue;

/**
 * Created by root on 3/10/16.
 */
public class GoogleCustomSearchResponseItem {
    private String imageLink;

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }




}
