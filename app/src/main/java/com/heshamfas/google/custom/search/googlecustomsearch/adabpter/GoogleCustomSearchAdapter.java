package com.heshamfas.google.custom.search.googlecustomsearch.adabpter;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.heshamfas.google.custom.search.googlecustomsearch.GoogleCustomSearch;
import com.heshamfas.google.custom.search.googlecustomsearch.GoogleCustomSearchPictureFragment;
import com.heshamfas.google.custom.search.googlecustomsearch.R;
import com.heshamfas.google.custom.search.googlecustomsearch.data.GoogleCustomSearchResponseItem;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

/**
 * Created by root on 3/11/16.
 */
public class GoogleCustomSearchAdapter extends RecyclerView.Adapter<GoogleCustomSearchAdapter.ItemViewHolder>{
    ArrayList<GoogleCustomSearchResponseItem> searchItems;
    private Activity mContext;
    private ImageLoader mImageLoader = GoogleCustomSearch.getInstance().getImageLoader();
    public GoogleCustomSearchAdapter(Activity context, ArrayList<GoogleCustomSearchResponseItem> searchItems){
        this.mContext = context;
        this.searchItems = searchItems;
    }
public ArrayList<GoogleCustomSearchResponseItem> getSearchList (){
    return searchItems;
}
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View viewItem = inflater.inflate(R.layout.google_search_item, null);
        return new ImageItemViewHolder(viewItem, this)  ;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if(searchItems.get(position) != null){
            GoogleCustomSearchResponseItem responseItem = searchItems.get(position);
            holder.populateItemView(responseItem);
        }
    }

    @Override
    public int getItemCount() {
        return searchItems.size();
    }

    public static abstract class ItemViewHolder extends RecyclerView.ViewHolder{
        View searchItemView;
        GoogleCustomSearchAdapter mAdapter;

        public ItemViewHolder(View itemView, GoogleCustomSearchAdapter adapter){
            super(itemView);
            this.searchItemView = itemView;
            this.mAdapter = adapter;
        }
        protected abstract void populateItemView(GoogleCustomSearchResponseItem currentItem);
    }

    public static class ImageItemViewHolder extends ItemViewHolder implements AdapterView.OnClickListener{
        NetworkImageView networkImageView;
        GoogleCustomSearchResponseItem currentItem;
        public ImageItemViewHolder(View itemView, GoogleCustomSearchAdapter adapter){
            super(itemView, adapter);
            networkImageView = (NetworkImageView)itemView.findViewById(R.id.niv_custom_search);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void populateItemView(GoogleCustomSearchResponseItem currentItem) {

            // imageLoader.get(imageLinke, ImageLoader.getImageListener(imageView, R.drawable.soccer, R.drawable.soccer));
            this.currentItem = currentItem;
            networkImageView.setImageUrl(currentItem.getImageLink(),mAdapter.mImageLoader);
        }

        @Override
        public void onClick(View v) {
            FragmentManager fm = mAdapter.mContext.getFragmentManager();
            GoogleCustomSearchPictureFragment pictureFragment = new GoogleCustomSearchPictureFragment();
            Bundle arguments = new Bundle();
            arguments.putString("url", currentItem.getImageLink());
            pictureFragment.setArguments(arguments);
            pictureFragment.show(fm, "fragment_edit_name");
        }
    }

}
