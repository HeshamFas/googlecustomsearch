package com.heshamfas.google.custom.search.googlecustomsearch.parser;

import com.heshamfas.google.custom.search.googlecustomsearch.data.GoogleCustomSearchResponse;
import com.heshamfas.google.custom.search.googlecustomsearch.data.GoogleCustomSearchResponseItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by root on 3/10/16.
 */
public class GoogleCustomSearchResponseParser {
    private static final String CUSTOM_SEARCH_RESULT = "customsearch#result";
    private static final String IS_NONLINEAR_AUTH = "nonlinearAuth";
    private static final String ITEMS = "items";
    private static final String LINK = "link";


    public static GoogleCustomSearchResponse parse(InputStream is) throws IOException, JSONException {
        GoogleCustomSearchResponse googleCustomSearchResponse = new GoogleCustomSearchResponse();
        ArrayList<GoogleCustomSearchResponseItem> customSearchResponseItems = new ArrayList<>();
        GoogleCustomSearchResponseItem searchItem;
        String result = convertStreamToString(is);
        if (result != null) {
            googleCustomSearchResponse.setIsSuccessfull(true);
            JSONObject mainResponse = new JSONObject(result);
            JSONArray itemsArray = mainResponse.getJSONArray(ITEMS);
            if(itemsArray!= null ){
            for (int i = 0; i < itemsArray.length(); i++) {
                JSONObject tempObject = itemsArray.getJSONObject(i);
                if (tempObject != null) {
                    searchItem = new GoogleCustomSearchResponseItem();
                    searchItem.setImageLink(tempObject.getString(LINK));
                    customSearchResponseItems.add(searchItem);
                }
            }}else {
                googleCustomSearchResponse.setIsSuccessfull(false);
            }
            googleCustomSearchResponse.setResponseItems(customSearchResponseItems);
        }else {
            googleCustomSearchResponse.setIsSuccessfull(false);
        }

        return googleCustomSearchResponse;
    }


    public static String convertStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } finally {
                is.close();
            }
        }
        return sb.toString();
    }
}

