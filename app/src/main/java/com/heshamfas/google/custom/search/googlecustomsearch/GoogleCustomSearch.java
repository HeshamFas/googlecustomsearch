package com.heshamfas.google.custom.search.googlecustomsearch;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.heshamfas.google.custom.search.googlecustomsearch.data.cache.BitmapLruCache;

import android.app.Application;
import android.content.Context;

/**
 * Created by root on 3/10/16.
 */
public class GoogleCustomSearch extends Application {
    private static ImageLoader mImageLoader;
   private static GoogleCustomSearch instance;
    /**
     * 20% of the heap goes to image cache. Stored in kiloibytes.
     */
    private static final int IMAGE_CACHE_SIZE_KB = (int) (Runtime.getRuntime().maxMemory() / 1024 / 5);

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        instance = this;
    }

public static GoogleCustomSearch getInstance(){
    return instance;
}

    public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(Volley.newRequestQueue(this), new BitmapLruCache(IMAGE_CACHE_SIZE_KB));
        }
        return mImageLoader;
    }
}
