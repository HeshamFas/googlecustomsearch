package com.heshamfas.google.custom.search.googlecustomsearch.data;

import java.util.ArrayList;

/**
 * Created by root on 3/10/16.
 */
public class GoogleCustomSearchResponse {

    ArrayList<GoogleCustomSearchResponseItem> responseItems ;
    boolean isSuccessfull;

    public boolean isSuccessfull() {
        return isSuccessfull;
    }

    public void setIsSuccessfull(boolean isSuccessfull) {
        this.isSuccessfull = isSuccessfull;
    }





    public ArrayList<GoogleCustomSearchResponseItem> getResponseItems() {
        return responseItems;
    }

    public void setResponseItems(ArrayList<GoogleCustomSearchResponseItem> responseItems) {
        this.responseItems = responseItems;
    }




}
