package com.heshamfas.google.custom.search.googlecustomsearch;

import com.heshamfas.google.custom.search.googlecustomsearch.adabpter.GoogleCustomSearchAdapter;
import com.heshamfas.google.custom.search.googlecustomsearch.data.GoogleCustomSearchResponse;
import com.heshamfas.google.custom.search.googlecustomsearch.data.GoogleCustomSearchResponseItem;
import com.heshamfas.google.custom.search.googlecustomsearch.parser.GoogleCustomSearchResponseParser;

import org.json.JSONException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class CustomSearchActivity extends AppCompatActivity implements View.OnClickListener{
    private static final int NUMBER_OF_IMAGES = 25 ; //multiple of 10s
    private ArrayList<GoogleCustomSearchResponseItem> mItems = new ArrayList<>();
    TextView mTextView;
    TextView mResultStatus;
    EditText mSearchEditText;
    Button mSearchButton;
    RecyclerView mImagesContainer;
    LinearLayout mResultLayout;
    RelativeLayout mNoResultLayout;
    GoogleCustomSearchAdapter mCustomSearchAdapter;
    private static final int NUMBER_OF_COLUMNS = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_custom_search);
        mTextView = (TextView) findViewById(R.id.text);
        mSearchEditText = (EditText)findViewById(R.id.et_custom_search);
        mSearchButton = (Button) findViewById(R.id.btn_custom_search);
        mSearchButton.setOnClickListener(this);
        mResultStatus =(TextView)findViewById(R.id.tv_custom_search_result);
        mResultLayout =(LinearLayout) findViewById(R.id.ll_custom_search_result);
        mNoResultLayout = (RelativeLayout) findViewById(R.id.rl_custom_search_no_result);
        mImagesContainer = (RecyclerView)findViewById(R.id.rv_image_container);
        //google search api allows only 10 image at each call
        GridLayoutManager layoutManager = new GridLayoutManager(this,NUMBER_OF_COLUMNS,GridLayoutManager.VERTICAL,false);
        mImagesContainer.setLayoutManager(layoutManager);
        mCustomSearchAdapter = new GoogleCustomSearchAdapter(this,mItems );
        mImagesContainer.setAdapter(mCustomSearchAdapter);
        showResultLayout();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_custom_search:
                String searchTerm = mSearchEditText.getText().toString();
                if(!TextUtils.isEmpty(searchTerm)){
                    int itemsCount = mItems.size();
                    if(itemsCount>0) {
                        mItems.clear();
                        mCustomSearchAdapter.notifyItemRangeRemoved(0, itemsCount);
                    }
                    generateSearch(searchTerm);
                }
                break;
        }
    }

    private  class GoogleCustomSearchCall extends AsyncTask<URL, Void, GoogleCustomSearchResponse> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected GoogleCustomSearchResponse doInBackground(URL... params) {
            GoogleCustomSearchResponse output=null;
            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) params[0].openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                conn.connect();
                output = GoogleCustomSearchResponseParser.parse(conn.getInputStream());
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                return output;
            }
        }


        @Override
        protected void onPostExecute(GoogleCustomSearchResponse result) {
           // Toast.makeText(CustomSearchActivity.this, result, Toast.LENGTH_LONG);
           // result = null;// testing
            if (result!=null && result.isSuccessfull() && result.getResponseItems().size()>0) {
                Log.d("CustomSearchActivity", "Success");
                showResultLayout();
                int prevItemsCount = mItems.size();
                mCustomSearchAdapter.getSearchList().addAll(result.getResponseItems());
                mCustomSearchAdapter.notifyItemRangeInserted(prevItemsCount ,result.getResponseItems().size());
                Log.d("CustomSearhActivity", "search item list size is " + mItems.size());

        }else {
                Log.d("CustomSearchActivity", "Result failed");
                showNoResultLayout();
            }
        }
    }
    private void generateSearch(String searchTerm){
        // google can return max 10 images per call.
        //making mutiple calls to get the desired number of images.
        int remainder = NUMBER_OF_IMAGES%10;
        int mutiplesOfTen = (NUMBER_OF_IMAGES - remainder)/10;
        for(int i = 1; i<=mutiplesOfTen; i++){
            sendSearch(searchTerm, 10);
        }
        if(remainder>0) {
            sendSearch(searchTerm, remainder);
        }
    }
    private void sendSearch(String searchString, int count) {
        String key = "AIzaSyCZTS7y9iWgez4BIQ7KBe8MA-J24Hi0v44";
        //String qry = "flower";
        try {
            /*&searchType=image*/
            int index = mItems.size()+1;
              URL url = new URL(
                  "https://www.googleapis.com/customsearch/v1?key=" + key
                            + "&cx=006449691909282340203:iirwolwzjn0&q=" + searchString + "&searchType=image&alt=json&imgSize=small&num=" + count +"&start=" + index);
            /*&cx=006449691909282340203:dohni2s-0_e*//*&imgSize=small*//**/
            /*URL url = new URL(
                    "https://www.googleapis.com/customsearch/v1?key="+ key+"&cx=013036536707430787589:_pqjad5hr1a&q=&q=flower&searchType=image&alt=json");*/
            new GoogleCustomSearchCall().execute(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        }
    private void showResultLayout() {
        mNoResultLayout.setVisibility(View.GONE);
        mResultLayout.setVisibility(View.VISIBLE);
        mSearchButton.setEnabled(true);
    }
    private void showNoResultLayout(){
        mNoResultLayout.setVisibility(View.VISIBLE);
        mResultLayout.setVisibility(View.GONE);
        mResultStatus.setText("No Result Found");
        mSearchButton.setEnabled(true);
    }

    private void showLoading(){
        mNoResultLayout.setVisibility(View.VISIBLE);
        mResultLayout.setVisibility(View.GONE);
        mResultStatus.setText("Loading ...");
        mSearchButton.setEnabled(false);
    }

}
